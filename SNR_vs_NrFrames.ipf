#pragma rtGlobals=3		// Use modern global access method and strict wave access.

Function SNRvsStartFrame(V_MovieLength)
	Variable V_MovieLength
	NewDataFolder /O root:Packages
	NewDataFolder /O root:Packages:SOFIEvaluator
	
	Open /D/R/T="????" /MULT=0 RefNum
	if (strlen(S_Filename) == 0)	// The user cancelled the dialog
		return -1
	endif
	Print S_Filename

	Variable i,j,cutoff
	Variable V_NumberOfFrames = 250

	Make /O/N=(ceil(V_MovieLength/V_NumberOfFrames)) W_ResolutionValues

	String DoAverage = "No"

	For(i=0;i<V_MovieLength;i+=250)
		SOFIevaluation_worker(S_fileName,i,V_NumberOfFrames,DoAverage,2)
		Wave ring
		For(j=0;j<dimsize(ring,0);j+=1)
			if(ring[j]<1.5)
				cutoff = ((1.5 - ring[j-1])/(ring[j]-ring[j-1]) + j)*.04
				print "SNR cutoff for ", i, " frames is ", cutoff
				W_resolutionValues[i/250] = 107 / cutoff
				break
			Endif
		EndFor
	Endfor
		
	WaveStats W_resolutionValues
	
	Print "The average resolution is ",V_avg, " with a SD of ", V_sdev

End

Function SNRvsFramess(start)
	variable start
	
	NewDataFolder /O root:Packages
	NewDataFolder /O/S root:Packages:SOFIEvaluator

	String pathstring = ""
	do
		Open /D/R/T="????" /MULT=0 RefNum
//		if (strlen(S_Filename) == 0)	// The user cancelled the dialog
//			return -1
//		endif
		pathstring += S_Filename
		pathstring += ";"
	While(strlen(S_Filename) != 0)
	pathstring = pathstring[0,(strlen(pathstring)-3)]
	print pathstring

	
	variable V_limit = 2501, V_interval = 100
	string DoAverage = "No"

	

	variable a
	string path
	For(a=0; a<ItemsInList(pathstring); a+=1)
		string foldername = "Cell_" + num2str(a+1)
		DFREF savedDF= GetDataFolderDFR()
		NewDataFolder/O/S $foldername
		Make /O /N=(floor(V_limit/V_interval)) W_numFrames, W_SNRcutoffs, W_SNRcutoffs_realspace
		path = StringFromList(a,pathstring)
		Variable i,j,cutoff
		For(i=V_interval;i<V_limit;i+=V_interval)
			SOFIevaluation_worker(path,start,i,DoAverage,2)
			//print foldername
			wave ring
			For(j=0;j<dimsize(ring,0);j+=1)
				if(ring[j]<1.5)
					cutoff = ((1.5 - ring[j-1])/(ring[j]-ring[j-1]) + j)*.04
					print "SNR cutoff for ", i, " frames is ", cutoff
					W_numFrames[(i/V_interval)-1] = i
					W_SNRcutoffs[(i/V_interval)-1] = cutoff
					W_SNRcutoffs_realspace[(i/V_interval)-1] = 107 / cutoff
					break
				Endif
			EndFor
		EndFor
		SetDataFolder savedDF
	EndFor
	
End