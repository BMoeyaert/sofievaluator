#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#include "ProgressViewer"
#include "SOFI Normalization"

Menu "Macros"
	Submenu "Evaluate your SOFI experiment"
		"SOFI Evaluation", /Q, SOFIevaluation_FromDisk()
		"Evaluate Top Localizer Viewer", /Q, SOFIevaluation_TopViewer()
		"Plot SOFI Evaluation", /Q, showplots()
		"Bulk SOFI Evaluation", /Q, BulkSOFIEvaluator()
	End
End

Function SOFIevaluation_FromDisk()
	variable refNum
	Open /D/R/T="????" /MULT=0 RefNum
	if (strlen(S_Filename) == 0)	// The user cancelled the dialog
		return -1
	endif
	
	SOFIevaluation(S_fileName)
End

Function SOFIevaluation_TopViewer()
	string windowName = GetNameOfTopLocalizerViewer()
	if (strlen(windowName) == 0)
		Abort "No localizer window appears to be open"
	endif
	
	DFREF df = root:Packages:Localizer:$windowName
	SVAR filePath = df:S_filePath
	SOFIevaluation(filePath)
End

Function SOFIevaluation(path)
	string path

	variable pixelSize = 107

	String S_setname, DoPlotting
	Variable start, delta,order,doAverage
	start = 200
	delta = 100
	NewDataFolder /O root:Packages
	NewDataFolder /O root:Packages:SOFIEvaluator

	prompt start, "Start from frame"
	prompt delta, "Number of frames to use"
	prompt order, "Order of the calculation", popup, "1;2;3"
	prompt S_setname, "Name of the dataset"
	prompt DoPlotting, "Do you want to plot the result as well?", popup, "Yes;No"
	prompt DoAverage, "Do you want to calculate an Average Intensity Trace (this could take some time)?", popup, "No;Yes;"	// note: don't change order of "no" and "yes"
	DoPrompt "Give parameters", start, delta, order, S_Setname, DoPlotting, DoAverage

	
	if (V_flag==0)
		NewDataFolder /O root:Packages:SOFIEvaluator:$S_Setname
		SetDataFolder root:Packages:SOFIEvaluator:$S_Setname
		Variable/G V_start = start
		Variable/G V_delta = delta
		doAverage -= 1 // convert "No" to zero and "Yes" to one
		SOFIevaluation_worker(path,start,delta,DoAverage,order,pixelSize)
		SetDataFolder root:
	else
		return -1
	endif
	
	if (stringmatch(DoPlotting,"Yes") == 1)
		SetDataFolder root:Packages:SOFIEvaluator:$S_Setname
		showplots()
		SetDataFolder root:
	endif
End


Function showplots()
	String S_DataPath = GetDataFolder(1)
	String S_DataName = GetDataFolder(0)
	if (stringmatch(S_DataName,"root") == 1)
		DoAlert /T="Elabakes!" 0, "Please set the current working directory to the correct folder"
	elseif (stringmatch(S_DataName,"Packages") ==1)
		DoAlert /T="Elabakes!" 0, "Please set the current working directory to the correct folder"
	else

		SOFI_img(S_DataPath, S_DataName)
		SNR_normal(S_DataPath, S_DataName)
		fourier_snr(S_DataPath, S_DataName)
		lagcurve(S_DataPath, S_DataName)
		SNR_plot(S_DataPath, S_DataName)
		FT_SNR_PLOT(S_DataPath, S_DataName)
		if(exists("W_AvgIntTrace")==1)
			AVG_INT(S_DataPath, S_DataName)
		endif
	endif
	Print "Done!"
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function filternoise(image,resolution)
	// image = SOFI image wave (make shure it is rescaled to the dimensions of the detector -2)
	// resolution = in units of pixelsize of the detector (nm / projected pixelsize) aka: zoveel keer de pixelsize (dus resolutie of 214nm wordt resolution = 2 voor 107nm pixelsize vh originele beeldje)
			//af te lezen in FT_SNR_plots
	Variable resolution
	WAVE image
	Variable sdev = 0.21233/resolution
	//sdev is chosen such that the FWHM in fourier space equals the "resolution" cutoff
	FFT  /DEST=aFT image
	Duplicate /O/C aFT, M_SOFI_filter //just make a copy of aFT to serve as rightly scaled container wave for M_SOFI_filter
	WAVE/C aFT,M_SOFI_filter
	M_SOFI_filter = exp(-(x^2+y^2)/(2*(sdev^2)))
	aFT *= M_SOFI_filter
	IFFT  /DEST=M_SOFI_filtered aFT
	Killwaves aFT, M_SOFI_filter
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function AVG_INT(S_DataPath, S_DataName)
	String S_DataPath, S_DataName
	Wave W_AvgIntTrace
	NVAR V_start, V_delta
	Display /K=1 /W=(607.5,521,1002,729.5) W_AvgIntTrace as S_DataName + ": Average Intensity Trace"
	Label left "Average intensity"
	Label bottom "frame number"
	SetAxis /E=1 /N=1 left, 0, *
	SetDrawEnv xcoord=bottom
	SetDrawLayer UserFront
	Drawline V_start,0,V_start,1
	SetDrawEnv xcoord=bottom
	SetDrawLayer UserFront
	Drawline V_start+V_delta,0,V_start+V_delta,1
End

Function SNR_normal(S_DataPath, S_DataName)
	String S_DataPath, S_DataName
	String S_DataFile = S_DataPath + "SNR_Image"
	Display /K=1 /W=(207,398,561,722) as S_DataName + ": SNR image"
	AppendImage/T $S_DataFile
	ModifyImage SNR_Image ctab= {0,10,Rainbow,0}
	ModifyImage SNR_Image minRGB=(0,0,0)
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=44
	ModifyGraph tick=3
	ModifyGraph mirror=2
	ModifyGraph nticks=10
	ModifyGraph minor=1
	ModifyGraph noLabel=2
	ModifyGraph fSize=8
	ModifyGraph standoff=0
	ModifyGraph tkLblRot(left)=90
	ModifyGraph btLen=3
	ModifyGraph tlOffset=-2
	SetAxis/A/R left
	ColorScale/C/N=text0/F=0/A=MC/X=57.00/Y=0.00 image=SNR_Image, logLTrip=0.0001
	ColorScale/C/N=text0 lowTrip=0.1
End

Function SOFI_img(S_DataPath, S_DataName)
	String S_DataPath, S_DataName
	String S_DataFile = S_DataPath + "M_SOFI"
	Display /K=1 /W=(206.25,44.75,560.25,368.75) as S_DataName + ": sofi image"
	AppendImage/T $S_DataFile
	ModifyImage M_SOFI ctab= {0,*,Grays,0}
	ModifyImage M_SOFI minRGB=(0,0,0)
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=44
	ModifyGraph tick=3
	ModifyGraph mirror=2
	ModifyGraph nticks=10
	ModifyGraph minor=1
	ModifyGraph noLabel=2
	ModifyGraph fSize=8
	ModifyGraph standoff=0
	ModifyGraph tkLblRot(left)=90
	ModifyGraph btLen=3
	ModifyGraph tlOffset=-2
	SetAxis/A/R left
	ColorScale/C/N=text0/F=0/A=MC/X=57.00/Y=0.00 image=M_SOFI, nticks=0
	ColorScale/C/N=text0 logLTrip=0.0001, lowTrip=0.1
End

Function lagcurve(S_DataPath, S_DataName)
	String S_DataPath, S_DataName
	String S_DataFile = S_DataPath + "W_temporal_decorrelation"
	Display /K=1 /W=(604.5,44.75,999,253.25) $S_DataFile as S_DataName + ": W_temporal_decorrelation"
	ModifyGraph mode(W_temporal_decorrelation)=3
	SetAxis left -20,100
	Label left "residual signal"
	Label bottom "lag (frames)"
	CurveFit /Q/NTHR=0/TBOX=768 exp_XOffset $S_DataFile /D 
	
End

Function SNR_plot(S_DataPath, S_DataName)
	String S_DataPath, S_DataName
	String S_DataFile = S_DataPath + "Sorted_SNR"
	wave /Z sortedSNR = $S_DataFile
	if (!WaveExists(SortedSNR))
		wave SNR_image = $(S_DataPath + "SNR_image")
		Duplicate /O SNR_image, Sorted_SNR
		Redimension /N=(DimSize(SNR_image, 0) * DimSize(SNR_image, 1)) Sorted_SNR//make a 1D wave with as many entries as there are pixels in the image
		sort sorted_SNR,sorted_SNR
		SetScale/I x 0,100,"", sorted_SNR
	endif
	wave sortedSNR = $S_DataFile
	Display /K=1 /W=(605.25,282.5,999.75,491) sortedSNR as S_DataName + ": Sorted_SNR"
End

Function fourier_snr(S_DataPath, S_DataName)
	String S_DataPath, S_DataName
	String S_DataFile = S_DataPath + "SNR_FT_image_smooth"
	Display /K=1/W=(1013.25,278,1242.75,598.25) as S_DataName + ": SNR_FT_image_smooth"
	AppendImage/T $S_DataFile
	ModifyImage SNR_FT_image_smooth ctab= {1,10,Grays,0}
	ModifyGraph margin(left)=14,margin(bottom)=14,margin(top)=14,margin(right)=54
	ModifyGraph tick=3
	ModifyGraph mirror=2
	ModifyGraph nticks(left)=10,nticks(top)=6
	ModifyGraph minor=1
	ModifyGraph noLabel=2
	ModifyGraph fSize=8
	ModifyGraph standoff=0
	ModifyGraph tkLblRot(left)=90
	ModifyGraph btLen=3
	ModifyGraph tlOffset=-2
	SetAxis/A/R left
	ColorScale/C/N=text0/F=0/A=MC/X=70.15/Y=2.99 image=SNR_FT_image_smooth, logLTrip=0.0001
	ColorScale/C/N=text0 lowTrip=0.1
End

Function FT_SNR_PLOT(S_DataPath, S_DataName)
	String S_DataPath, S_DataName
	String S_DataFile1 = S_DataPath + "ring"
	String S_DataFile2 = S_DataPath + "scale"
	Display /K=1 /W=(1011.75,32.75,1406.25,237.5) $S_DataFile1,$S_DataFile2 as S_DataName + ": FT_SNR_plots"
	ModifyGraph rgb(scale)=(0,0,0)
	SetAxis left 0,*
	Label left "SNR";DelayUpdate
	Label bottom "frequency (1/px)"
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function SOFIevaluation_worker(path,nFramesToSkip,nFramesToUse,DoAverage,Order,pixelSize, [comb, wantSortedSNR])
	String path
	Variable nFramesToSkip,nFramesToUse,doAverage,order
	variable pixelSize // in nm
	variable comb, wantSortedSNR	// sorting can be slow
	
	if (ParamIsDefault(comb))
		comb = 1e50
	endif
	if (ParamIsDefault(wantSortedSNR))
		wantSortedSNR = 1
	endif
	
	// read ccd data and do SOFI calculation
	ReadCCDImages /Q /O /S=(nFramesToSkip) /C=(nFramesToUse) path
		WAVE M_CCDFrames
	newsofi /Q /JACK /ORDR=(order) /COMB=(comb) "M_CCDFrames" //COMB: fast = 1 (old code), good=10, possibly better=50, maximum=1e50
	WAVE M_SOFI,M_SOFI_jack
	
	variable res = EstimateResolution(M_CCDFrames, M_SOFI, M_SOFI_jack, doAverage, pixelSize, comb, wantSortedSNR)
	KillWaves /Z M_CCDFrames, M_SOFI_jack
	
	return res
End
		
Function EstimateResolution(M_CCDFrames, M_SOFI, M_SOFI_jack, doAverage, pixelSize, comb, wantSortedSNR)
	wave M_CCDFrames, M_SOFI, M_SOFI_jack
	variable doAverage, pixelSize, comb, wantSortedSNR
		
	variable size = dimsize(M_SOFI,0)  //measure (in number of pixels) the width of the SOFI image. only works for square images further down
	variable nFramesToUse = DimSize(M_SOFI_jack, 2)
	
	//calculate real-space SNR image
	//first calculate Noise image
		ImageTransform averageImage, M_SOFI_jack //make an average and the SD of the M_SOFI_jack image stack
			wave M_AveImage, M_StdvImage //contains images with in each pixel the average value and SD, respectively
		Duplicate /O M_StdvImage, M_sdev_SOFI //just a renaming procedure
			wave M_sdev_SOFI
		M_sdev_SOFI *= sqrt(nFramesToUse - 1) //change SD of the resampled stack to the SD of the original SOFI image
	//then make a copy of the Signal and divide the copy by the noise
		duplicate/O M_SOFI, SNR_image
		SNR_image /= M_sdev_SOFI  //SNR_image is the signal to noise ratio of the SOFI image
	//smoothen up
		Smooth/DIM=0 1, SNR_image
		Smooth/DIM=1 1, SNR_image
	//kill junk
		Killwaves M_AveImage,M_StdvImage
	
	//make cumulative real-space SNR distribution
	if (wantSortedSNR)
		Duplicate /O SNR_image, Sorted_SNR
		Redimension /N=(size*size) Sorted_SNR//make a 1D wave with as many entries as there are pixels in the image
		sort sorted_SNR,sorted_SNR
		SetScale/I x 0,100,"", sorted_SNR
	endif
	
	//measure signal decay on time-lag
	measure_lag(M_CCDFrames)
	
	//make fourier-space SNR-frequency plot
	//first calculate Signal image
		FT_RealComp(M_SOFI)
			wave M_SOFI_FT //This is the real component of the FT of M_SOFI
			M_SOFI_FT = abs(M_SOFI_FT) //We only work with the absolute values
	//then calculate the Noise image	
		fourierstack2(M_SOFI_jack) //transforms the stack of resampled images into a stack with the FT of each image
			wave M_SOFI_FT_jack
		ImageTransform averageImage, M_SOFI_FT_jack //make the average and the SD of M_FT_SOFI_jack image stack
			wave M_AveImage, M_StdvImage
		Duplicate /O M_StdvImage, M_sdev_SOFI_FT
		M_sdev_SOFI_FT *= sqrt(nFramesToUse - 1) //here the SD of the FT of the SOFI image is calculated (see JackKnive theory)
	//then make a copy of the Signal image and divide the copy by the noise	
		Duplicate /O M_SOFI_FT, SNR_FT_image
		SNR_FT_image /= M_sdev_SOFI_FT //this calculates the SNR of the FT by dividing the real comp of the FT of the SOFI image by the SNR of the resampled stack
	
	//make a wave of the ring averages versus the spatial frequencies
		ringaverage2(SNR_FT_image,50)
		wave ring
		Duplicate /O ring,scale
		scale = 1.5 //this is just the cut-off line
	
	//smoothen up
		Duplicate /O SNR_FT_image, SNR_FT_image_smooth
		Smooth/DIM=0 15, SNR_FT_image_smooth  ////////////////////why 15 smoothing iterations??? //////////////////////
		Smooth/DIM=1 15, SNR_FT_image_smooth
	//kill junk	
		killwaves /Z M_AveImage, M_StdvImage
		
	//now also calculate an average intensity trace
	if (DoAverage)
		MatrixOP /O W_AvgIntTrace = sumCols(sumRows(M_CCDFrames) / (size * size)
		Redimension /N=(DimSize(M_CCDFrames, 2)) W_AvgIntTrace
	endif
	
	ring = snrm2snr(ring)
	SNR_FT_image_smooth = snrm2snr(SNR_FT_image_smooth)
	
	//kill junk
	Killwaves /Z M_sdev_SOFI, M_SOFI_FT_jack, M_SOFI_FT, M_sdev_SOFI_FT, SNR_FT_image //comment out for debugging
	
	return Calculate_resolution(ring, pixelSize)
End

Function FT_RealComp(srcWave)
	wave srcWave
	//3=>2
	FFT /OUT=2 /DEST=M_SOFI_FT srcWave //FT_SNR (which will later become the SNR of the FT of the SOFI image) is now the real component of the FT of M_SOFI (=the sofi image)
End

Function measure_lag(M_CCDFrames)
	wave M_CCDFrames //M_CCDFrames are the input frames
	Variable nRows = dimsize(M_CCDFrames,0)
	Variable nCols = dimsize(M_CCDFrames,1)
	MAKE/O/D/N=(10) W_temporal_decorrelation = 0
	MAKE /FREE /D/N=(50) W_trace1,W_trace2
	Variable row, col, trial, t, x1, x2, xx, lag, temp
	Make /FREE /N=41 W_TraceProduct
	For (trial=0; trial<1000; trial+=1)//trial is just for counting
		row = floor(UniformRand() * (nRows-1))
		col = floor(UniformRand() * nCols)
		W_trace1 = M_CCDFrames[row][col][p]
		W_trace2 = M_CCDFrames[row+1][col][p]
		x1 = mean(W_trace1, 0, 40)
		For(lag=0;lag<10;lag+=1)		
			x2 = mean(W_trace2, lag, 40+lag)
			W_TraceProduct = W_trace1[p] * W_trace2[p+lag]
			xx = mean(W_TraceProduct, 0, 40)
			W_temporal_decorrelation[lag] += xx - (x1 * x2) // covariance of pixel (row,col) and (row+1,col)
		EndFor
	EndFor
	temp = W_temporal_decorrelation[0]
	W_temporal_decorrelation /= temp * 0.01//cross-cumulant versus time-lag expressed in % (with 100% at timelag 0)
End

Function fourierstack2(inputstack)
	wave inputstack
	Variable maxi = dimsize(inputstack,0)
	Variable maxj = dimsize(inputstack,1)
	Variable maxt = dimsize(inputstack,2)
	Variable FTmaxi = (maxi/2) + 1
	Make/O/D/N=(FTmaxi,maxj,maxt) M_SOFI_FT_jack
	Make /D/FREE/N=(maxt) W_Dummy
	MultiThread W_Dummy = FourierStackWorker2(p, inputstack, M_SOFI_FT_jack)
End

ThreadSafe Function FourierStackWorker2(imageIndex, inputstack, FTdata)
	variable imageIndex
	wave inputstack, FTdata
	ImageTransform /P=(imageIndex) getPlane, inputStack
	wave M_ImagePlane
	//3=>2
	FFT  /OUT=2 /DEST=tempFTframe M_ImagePlane
	ImageTransform /P=(imageIndex) /D=tempFTFrame setPlane, FTData
	return 0
End

Function /WAVE DeconvolveStackInPlace(M_InputStack, M_PSF, nIterations, [M_StartingImages])
	wave M_InputStack, M_PSF
	variable nIterations
	wave /Z M_StartingImages
	
	variable nImages = DimSize(M_InputStack, 2)
	Make /B/FREE /N=(nImages) W_Dummy
	MultiThread W_Dummy = DeconvolveStackInPlaceWorker(p, M_InputStack, M_PSF, nIterations, M_StartingImages=M_StartingImages)
End

ThreadSafe Function DeconvolveStackInPlaceWorker(imageIndex, M_Input, M_PSF, nIterations, [M_StartingImages])
	variable imageIndex
	wave M_Input, M_PSF
	variable nIterations
	wave /Z M_StartingImages		// for incremental calculation of the iterations
	
	ImageTransform /P=(imageIndex) getPlane, M_Input
	wave M_ImagePlane
	M_ImagePlane = max(M_ImagePlane, 0)
	if (!WaveExists(M_StartingImages))
		ImageRestore /ITER=(nIterations) psfWave=M_PSF, srcWave=M_ImagePlane
		wave M_Reconstructed
		ImageTransform /P=(imageIndex) /D=M_Reconstructed setPlane, M_Input
	else
		MatrixOP /FREE M_StartingImage = M_StartingImages[][][imageIndex]
		ImageRestore /ITER=(nIterations) psfWave=M_PSF, srcWave=M_ImagePlane, startingImage=M_StartingImage
		wave M_Reconstructed
		ImageTransform /P=(imageIndex) /D=M_Reconstructed setPlane, M_StartingImages
	endif
	return 0
End

Function ringaverage2(inputFFT,steps)
	wave inputFFT
	Variable steps
	
	Variable highestfrequency = dimoffset(inputFFT,0) + (dimsize(inputFFT,0) - 1)*dimdelta(inputFFT,0)
	Make/O/N=(steps) ring=0, tempcount=0
	SetScale /P x, highestfrequency/(2*steps), highestfrequency/steps, ring
	variable row,col, xx, yy, ringIndex
	For (col=0; col<dimsize(inputFFT,1); col+=1)
		yy = DimOffset(inputFFT, 1) + col * DimDelta(inputFFT, 1)
		For (row=0; row<dimsize(inputFFT,0); row+=1)
			xx = DimOffset(inputFFT, 0) + row * DimDelta(inputFFT, 0)
			ringIndex = floor(sqrt(xx^2 + yy^2) / DimDelta(ring, 0))
			if (ringIndex < steps)
				ring[ringIndex] += inputFFT[row][col]
				tempcount[ringIndex] += 1
			endif
		EndFor
	EndFor
	ring /= tempcount //calculate average of value in ring
	Killwaves tempcount
End

Function snr2snrm(snr)
	Variable snr
	Variable snrm =sqrt(2/pi) * exp(-(snr*snr)/2) + snr*(1-2*StatsNormalCDF((-snr), 0, 1)) //here we calculate the numeric values of the measured SNR (snrm) vs the real SNR (snr)
	Return snrm
End

constant kCalibrationVersion = 1	// update if the calibration format is changed
Function /WAVE FetchCalibration()
	if (WaveExists(root:Packages:SOFIevaluator:calibration) && (NumberByKey("CalibrationVersion", Note(root:Packages:SOFIevaluator:calibration)) == kCalibrationVersion))
		return root:Packages:SOFIevaluator:calibration
	endif
	NewDataFolder /O root:Packages
	NewDataFolder /O root:Packages:SOFIevaluator
	Make/O/N=2400 root:Packages:SOFIevaluator:calibration
	wave calibri = root:Packages:SOFIevaluator:calibration
	variable i,j
	For(i=0; i < DimSize(calibri, 0); i+=1)
		For(j=0; ; j+=0.001)
			if (snr2snrm(j) > 0.798 + (i/1000))
				calibri[i] = j
				break
			EndIf
		EndFor
	EndFor
	
	Note calibri, "CalibrationVersion:" + num2istr(kCalibrationVersion) + ";"
	return calibri
End

Function snrm2snr(snrm)
	Variable snrm
	Variable snr
	snrm -= 0.798
	snrm *= 1000
	snrm = round(snrm)
	if (snrm < 0)
		snr = 0
	Elseif (snrm > 2399)
		snr = (snrm / 1000) + 0.798
	Else
		wave calibri = FetchCalibration()
		snr = calibri[snrm]
	EndIf
	Return snr
End

Function /WAVE DeconvolutionPSF(order)
	variable order
	variable psfStdDev = 1.5		// random assumption
	
	Make /N=(13,13) /D /FREE M_DeconvolutionKernel
	SetScale /P x, -floor(13 / 2) / order, 1 / order, M_DeconvolutionKernel
	SetScale /P y, -floor(13 / 2) / order, 1 / order, M_DeconvolutionKernel
	
	M_DeconvolutionKernel = (StatsNormalPDF(sqrt(x^2 + y^2), 0, psfStdDev))^order
	
	return M_DeconvolutionKernel
End

Static Function UniformRand()
	return enoise(0.5) +0.5
End

////////////////////Function for evaluating lots of datasets from a single label//////////////////////////////////////

Function BulkSOFIEvaluator()

	variable pixelSize = 107
	
	//Get the parameters for the analysis
	//Plotting is set to No and Average to Yes. This should normally not be changed
	String path, S_setname = "testset"
	String DoPlotting = "No"
	variable DoAverage = 1
	String Debleach = "No"
	String Non_Debleach = "Yes"
	Variable start, order
	String delta = "250; 500"
	start = 750
	
	prompt start, "Start from frame"
	prompt delta, "Number of frames to use"
	prompt order, "The order of the calculation", popup, "1;2;3"
	prompt S_setname, "Name of the dataset"
	prompt Non_Debleach, "Do you want to process NON-DEBLEACHED movies?", popup, "Yes;No"
	prompt Debleach, "Do you want to process DEBLEACHED movies?", popup, "Yes;No"
	DoPrompt "Give parameters", start, delta, order, S_Setname, Non_Debleach, Debleach
		
	if (V_flag==1)
		return -1
	endif
	
	//Select path in which to search for data files
	NewPath /O /Q DataSetPath
	if (V_flag != 0)
		return 0
	endif
	
	//Make the SOFIEvaluator Folder	
	NewDataFolder /O root:Packages
	NewDataFolder /O /S root:Packages:SOFIEvaluator
	
	//Make a string containing the names of the subfolders (normally one per label) in the DataSetPath folder
	String S_labels = IndexedDir(DataSetPath, -1, 0)
	Variable nLabels = ItemsInList(S_labels)
	
		//set how many images to do the analysis on
		DFREF saveDFR = GetDataFolderDFR()
		
		//Make a text wave containing the label names
		Make/O/T /N=(nLabels) W_LabelNames
		
		Variable l
		for (l=0; l<ItemsInList(delta); l+=1) 
			Variable ChosenDelta = str2num(StringFromList(l,delta))
			ProgressWindow("nFrames", l+1 , ItemsInList(delta))
			
			//make a folder named after the number of frames we'll use
			String DeltaFolderName = "DF_" + num2str(ChosenDelta) + "frames" 
			NewDataFolder /O /S $DeltaFolderName
			
			//make the output files: one for the debleached data and one for the non-debleached data
			String outputNDB = "nonDB_" + num2str(ChosenDelta) + "fr"
			String outputNDB_bleach = "nonDB_" + num2str(ChosenDelta) + "fr_bleaching"
			Make /O /N=(nLabels,20) $outputNDB=nan, $outputNDB_bleach=nan
			Wave W_outputNDB = $outputNDB
			Wave W_outputNDB_bleach = $outputNDB_bleach
			
			String outputDB = "DB_" + num2str(ChosenDelta) + "fr"
			String outputDB_bleach = "DB_" + num2str(ChosenDelta) + "fr_bleaching"
			Make /O /N=(nLabels,20) $outputDB=nan, $outputDB_bleach=nan
			Wave W_outputDB = $outputDB
			Wave W_outputDB_bleach = $outputDB_bleach
			
			//set on which label to perform the calculation
			DFREF saveDF2R = GetDataFolderDFR()
			Variable k
			for(k=0; k<nLabels; k+=1)
				ProgressWindow("Label", k+1 , nLabels)
				
				//make a folder named after the label we'll use
				string LabelName = IndexedDir(DataSetPath, k, 0)
				NewDataFolder /O /S $LabelName
				W_LabelNames[k] = LabelName
				
				//make a string containing the names of all the files we'll calculate on
				PathInfo DataSetPath
				string S_LabelPath = S_Path + StringfromList(k,S_labels)
				NewPath /Q LabelPath, S_LabelPath
				string dataFileNames = IndexedFile(LabelPath, -1, ".his")
				dataFileNames += IndexedFile(LabelPath, -1, ".tif")
				dataFileNames += IndexedFile(LabelPath, -1, ".btf")
				variable nDataFiles = ItemsInList(dataFileNames)
				KillPath /Z LabelPath
				
				//We'll start to go through all the data files and perform the Workerfunction
				variable i,j,cutoff
				string dataFilePath, S_fileName
				for (i = 0; i < nDataFiles; i+=1)
					ProgressWindow("Cell", i+1 , nDataFiles)
					DFREF saveDF3R = GetDataFolderDFR()
					
					//make a folder to store Worker data in (non-debleached)
					String dataFileNameNDB = StringFromList(i, dataFileNames)
					NewDataFolder /O /S $dataFileNameNDB
					
					//do the Worker function
					dataFilePath = S_LabelPath + ":" + dataFileNameNDB
					Strswitch (Non_Debleach)
						case "Yes":
							SOFIevaluation_worker(dataFilePath,start,ChosenDelta,DoAverage,order,pixelSize, wantSortedSNR=0)
							wave ring,W_temporal_decorrelation
							Print "Done evaluation_worker on " + dataFilePath + ", from " + num2str(start) + "th frame, " + num2str(ChosenDelta) + "frames long. Averaging: " + num2str(DoAverage)
							//get the data from the worker and store in correct file
							W_outputNDB[k][i] = Calculate_resolution(ring, pixelSize)
							W_outputNDB_bleach[k][i] = Calculate_bleaching(W_temporal_decorrelation)
							break
						case "No":
							Print "No evaluation_worker done on " + dataFilePath
							break
					endswitch
					SetDataFolder saveDF3R
					
					//perform an extra step if the debleaching option was selected
					Strswitch (Debleach)
						case "Yes":
							//make a folder to store Worker data in (debleached)
							String dataFileNameDB = "DB_" + StringFromList(i, dataFileNames)
							NewDataFolder /O /S $dataFileNameDB
						
							//do the debleaching
							dataFilePath = S_LabelPath + ":" + StringFromList(i, dataFileNames)
							ReadCCDImages /O/Q/DEST=M_CCDFramesBleachCorrect /S=(0) dataFilePath
							wave M_CCDFramesBleachCorrect
							FilterProcess(M_CCDFramesBleachCorrect, 31, "filterfir", "dontmodify")
							Wave M_Filtered
							KillWaves /Z M_CCDFramesBleachCorrect
							Print "Debleached " + StringFromList(i, dataFileNames)
						
							//do the worker function
							String DebleachedMovie = "M_Filtered"
							SOFIevaluation_worker(DebleachedMovie,start,ChosenDelta,DoAverage,order,pixelSize,wantSortedSNR=0)
							wave ring,W_temporal_decorrelation
							Print "Done evaluation_worker on debleached " + dataFilePath + ", from " + num2str(start) + "th frame, " + num2str(ChosenDelta) + "frames long. Averaging: " + num2str(DoAverage)
						
							//get the data from the worker and store in correct file
							W_outputDB[k][i] = Calculate_resolution(ring, pixelSize)
							W_outputDB_bleach[k][i] = Calculate_bleaching(W_temporal_decorrelation)						
						
							KillWaves M_Filtered
																
							break
						case "No":
							Print "No evaluation_worker done on debleached " + dataFilePath
							break
					endswitch
					SetDataFolder saveDF3R
				endfor
				SetDataFolder saveDF2R
			endfor	
			SetDataFolder saveDFR
		endfor
End
	

Function Calculate_resolution(ring, pixelSize)
	Wave ring
	variable pixelSize // in nm
	
	FindLevel /Q ring, 1.5
	if (V_flag)
		return NaN
	else
		return pixelSize / V_levelX
	endif
End

Function Calculate_bleaching(inputfile)	//Calculate the SOFI signal due to bleaching
	wave inputfile
	Variable V_fitError = 0
	CurveFit /Q/NTHR=0/TBOX=768 exp_XOffset inputfile /D
	Wave W_coef
	variable offset = W_coef[0]
	return offset
End

Function Plot_SOFIEvaluator(outputWave,labelwave,titleStr,res)
	wave outputWave, labelwave,res
	string titleStr
	
	Duplicate /O outputWave, W_Yvalues
	wave W_Yvalues
	W_Yvalues = p
	Make /O /N=(dimsize(outputWave,0)) LabelValues = p
	
	variable i
	Display outputWave[0][] vs W_Yvalues[0][] as titleStr
	for(i=1;i<dimsize(outputWave,0);i+=1)
		AppendToGraph outputWave[i][] vs W_Yvalues[i][]
		DoUpdate
	endfor
	ModifyGraph userticks(bottom)={LabelValues,labelwave}
	ModifyGraph mode=3,marker=19,tkLblRot(bottom)=90,msize=1.5
	SetAxis left 0,1400
	SetAxis bottom -0.1875,15.1875
	
	AppendToGraph res
	ModifyGraph width=368.504,height=198.425	
	ModifyGraph rgb=(0,0,0)
	ModifyGraph axOffset(bottom)=-1.1875
	ModifyGraph tkLblRot(bottom)=50
	Label left "Jackknife resolution (nm)"
	TextBox/A=RT/F=0/H={0,6,10} /X=10 /Y=5 titleStr
	SavePICT/EF=1/E=-3 as titleStr
End

Static Function /S GetNameOfTopLocalizerViewer()

	string windowName
	variable i
	
	for (i = 0; ; i+=1)
		windowName = WinName(i, 64, 1)
		if (strlen(windowName) == 0)
			return ""
		endif
		if (GrepString(windowName, "LocalizerViewer[0-9]*") == 1)
			return windowName
		endif
	endfor
End
